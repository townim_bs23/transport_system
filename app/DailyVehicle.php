<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyVehicle extends Model
{
    protected $fillable = [
    	'owner_id',
    	'vehicle_id',
    	'driver_id',
    	'issued_date'
    ];

    //vehicle
    public function vehicle()
    {
        return $this->belongsTo('App\RegisteredVehicle', 'vehicle_id');
    }

    //driver
    public function driver()
    {
        return $this->belongsTo('App\RegisteredDriver', 'driver_id');
    }

    //user information
    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }
}
