<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationInformation extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'father_name',
        'mother_name',
        'spouse_name',
        'gender',
        'email',
        'phone_number',
        'birth_date',
        'permanent_address',
        'present_address',
        'citizen',
        'nid',
        'passport',
        'image'
    ];

    //vehicle application
    public function vehicleApplications()
    {
        return $this->hasMany('App\VehicleApplication', 'application_id');
    }

    //driver application
    public function driverApplications()
    {
        return $this->hasMany('App\DriverApplication', 'application_id');
    }

    //vehicle application
    public function registeredUserInformation()
    {
        return $this->hasMany('App\RegisteredUserInformation', 'information_id');
    }

}
