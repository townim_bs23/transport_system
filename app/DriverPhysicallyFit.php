<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverPhysicallyFit extends Model
{
    protected $fillable = [
    	'driver_application',
        'headache',
        'visual_condition',
        'traffic_sign',
        'hearing'
    ];

    //driver application
    public function information()
    {
        return $this->belongsTo('App\DriverApplication', 'driver_application');
    }
}
