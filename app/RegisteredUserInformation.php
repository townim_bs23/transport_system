<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredUserInformation extends Model
{
    protected $fillable = [
    	'information_id',
    	'user_id'
    ];

    //application information
    public function driverInformation()
    {
        return $this->belongsTo('App\ApplicationInformation', 'information_id');
    }

    //user information
    public function userInformation()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
