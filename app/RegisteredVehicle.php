<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredVehicle extends Model
{
    protected $fillable = [
    	'vehicle_id',
        'user_id',
        'vehicle_registration_no',
        'issued_date',
        'expired_date'
    ];

    //vehicle information
    public function vehicleInformation()
    {
        return $this->belongsTo('App\VehicleApplication', 'vehicle_id');
    }

    //user information
    public function userInformation()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    //daily vehicle owned to this id
    public function dailyVehicle()
    {
        return $this->hasMany('App\DailyVehicle', 'vehicle_id');
    }
}
