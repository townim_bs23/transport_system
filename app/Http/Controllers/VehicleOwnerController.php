<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\{AccidentLocation, ApplicationInformation, DailyVehicle, DriverApplication, DriverPhysicallyFit, RegisteredDriver, RegisteredUserInformation, RegisteredVehicle, User, Role, VehicleApplication};

use Carbon\Carbon;

class VehicleOwnerController extends Controller
{
	
    public function viewAllVehicles(){
    	/*
        * IMPORTANT POINTS IN VEHICLE OWNER SECTION
        * 1. vehicle registration form will be fill up with owner user name or new application information
        * 2. if username exists then the owner will be that user  
        * 3. BRTA will select which vehicle is fit or not, then only fit vehicles will be registered by BRTA
        * 4. those whose request of registration is pending, their registrations will be confirmed by sms or email
        * 5. only registered  vehicle will be viewed in vehicle owner section
        * 6. after expired date a registered vehicle will be unfit
        */

        // all_vehicles variable will store the information from registeredVehicle table where the user id of this table will be matched with the user id of the vehicle owner who is currently logged in. Because here only those vehicles info will be shown which are owned by the logged in malik
        $all_vehicles = RegisteredVehicle::where('user_id', Auth::user()->id)->latest()->get();
        // here in owner.all_vehicles, owner=folder name and all_vehicles=page name
	    return view('adminlte::owner.all_vehicles', compact('all_vehicles'));
    }
    
    public function viewRegisteredVehicles(){
        // daily_vehicles variable will store the information from dailyVehicle table where the owner id of this table will be matched with the user id of the vehicle owner who is currently logged in. Because here only those vehicles info will be shown which are registered on a particular day and owned by the logged in malik
        $daily_vehicles = DailyVehicle::where('owner_id', Auth::user()->id)
                                        ->latest()
                                        ->get();
        // here in owner.daily_registered, owner=folder name and daily_registered=page name
        return view('adminlte::owner.daily_registered_vehicles', compact('daily_vehicles'));
    }

    //view form of daily vehicle registration
    public function getDailyVehicle(){
        // vehicle_list variable will store the information from registeredVehicle table where the user id of this table will be matched with the user id of the vehicle owner who is currently logged in. Because in the dropdown menu of Your vehicle in the daily registraton form the vehicle list will be mentioned which are already owned by the logged in malik
        $vehicle_list = RegisteredVehicle::where('user_id', Auth::user()->id)->get();
        // vehicle_list variable will store the information from registeredDriver table. Because in the dropdown menu of Driver in daily registraton form the driver list will be mentioned who are already registered (means their application are accepted) so that the vehicle owner can assign any driver for his vehicle
        $driver_list = RegisteredDriver::all();
        // here in owner.register_daily_vehicle, owner=folder name and register_daily_vehicle=page name
        return view('adminlte::owner.register_daily_vehicle', compact(
            'vehicle_list',
            'driver_list'
        ));
    }

    //submit form of daily vehicle registration
    public function submitDailyVehicle(Request $request){
        $this->validate($request, [
         // as vehicle and driver will be taken as input
            "vehicle" => "required",
            "driver" => "required"
        ]);
       // daily_vehicle variable will store the input info of daily vehicle registration. here in the dailyVehicle table a new info will be inserted. Currently logged in vehicle owner's user id will be inserted in owner_id attribute of dailyVehicle table. Current time will be inserted in issued_date attribute of dailyVehicle table. Input of Your vehicle and Driver will be inserted in vehicle_id and driver_id attribute of dailyVehicle table.
        $daily_vehicle =  DailyVehicle::create([
            'owner_id' => Auth::user()->id,
            'vehicle_id' => $request->vehicle,
            'driver_id' => $request->driver,
            'issued_date' => Carbon::now()->toDateString()
        ]);

        return back()->with(['success' => 'Saved Successfully']);
    }
}
