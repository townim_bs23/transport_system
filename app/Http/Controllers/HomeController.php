<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{ApplicationInformation, DriverApplication, VehicleApplication, RegisteredVehicle, RegisteredDriver, DailyVehicle, User};


class HomeController extends Controller
{
    //view home page of search daily vehicle
    public function viewHome(){
        $daily_vehicles = DailyVehicle::latest()->get();
        return view('adminlte::home.welcome', compact('daily_vehicles'));
    }

    //view application form of driving licence
    public function viewDriverForm(){
        return view('adminlte::home.driving_licence');
    }

    //view application form of vehicle registration
    public function viewVehicleForm(){
        return view('adminlte::home.vehicle_registration');
    }

    //view dashboard
    public function viewDashboard(){   
        return view('adminlte::dashboard');
    }

    //save data of driver registration with validation 
    public function saveDriverInfo(Request $request){
        //dd($request->present_address);
        $this->validate($request, [
            "first_name" => "required",
            "last_name" => "required",
            "father_name" => "required",
            "mother_name" => "required",
            "gender" => "required",
            "birth_date" => "required|date",
            "nid" => "required",
            "present_address" => "required",
            "permanent_address" => "required",
            "email" => "required|email",
            "phone_number" => "required|numeric",
            "citizen" => "required",
            "vehicle_class" => "required",
            "application_type" => "required",
            "licence_type" => "required",
            "bank_transaction_no" => "required|numeric"
        ]);

        $applicationInfo = ApplicationInformation::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'spouse_name' => $request->spouse_name,
            'gender' => $request->gender,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'birth_date' => $request->birth_date,
            'permanent_address' => $request->permanent_address,
            'present_address' => $request->present_address,
            'citizen' => $request->citizen,
            'nid' => $request->nid,
            'passport' => $request->passport
        ]);

        $applicationInfo->driverApplications()->create([
            'vehicle_class' =>$request->vehicle_class,
            'application_type' =>$request->application_type,
            'licence_type' =>$request->licence_type,
            'bank_transaction_no' =>$request->bank_transaction_no,
            'police_verified' => 0,
        ]);

        return back()->with(['success' => 'Saved Successfully']);
    }

    //save data of vehicle registration with validation 
    public function saveVehicleInfo(Request $request){
        $applicationInfoId = null;
        if($request->username!=null){
            $this->validate($request, [
                "fuel_used" => "required|numeric",
                "tyre_size" => "required|numeric",
                "class" => "required",
                "chasis_no" => "required",
                "horse_power" => "required|numeric",
                "rpm" => "required|numeric",
                "seats" => "required|numeric",
                "cubic_capacity" => "required|numeric",
                "standee_no" => "required|numeric",
                "no_of_cylinders" => "required|numeric",
                "weight" => "required|numeric",
                "engine_no" => "required|numeric",
                "body_type" => "required",
                "makers_country" => "required",
                "maker_name" => "required",
                "year_of_manufacture" => "required|numeric"
            ]);
            $applicationInfo = User::where('name', $request->username)->first();
            if($applicationInfo!=null){
                $applicationInfoId = $applicationInfo->applicationInformation->information_id;
            }else{
                return back()->with(['no-user-error' => 'No such username exists']);
            }
        }else{
            $this->validate($request, [
                "first_name" => "required",
                "last_name" => "required",
                "father_name" => "required",
                "mother_name" => "required",
                "gender" => "required",
                "birth_date" => "required|date",
                "nid" => "required",
                "present_address" => "required",
                "permanent_address" => "required",
                "email" => "required|email",
                "phone_number" => "required|numeric",
                "citizen" => "required",
                "fuel_used" => "required|numeric",
                "tyre_size" => "required|numeric",
                "class" => "required",
                "chasis_no" => "required",
                "horse_power" => "required|numeric",
                "rpm" => "required|numeric",
                "seats" => "required|numeric",
                "cubic_capacity" => "required|numeric",
                "standee_no" => "required|numeric",
                "no_of_cylinders" => "required|numeric",
                "weight" => "required|numeric",
                "engine_no" => "required|numeric",
                "body_type" => "required",
                "makers_country" => "required",
                "maker_name" => "required",
                "year_of_manufacture" => "required|numeric",
                "image" => "required"
            ]);
            $applicationInfo = ApplicationInformation::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'father_name' => $request->father_name,
                'mother_name' => $request->mother_name,
                'spouse_name' => $request->spouse_name,
                'gender' => $request->gender,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'birth_date' => $request->birth_date,
                'permanent_address' => $request->permanent_address,
                'present_address' => $request->present_address,
                'citizen' => $request->citizen,
                'nid' => $request->nid,
                'passport' => $request->passport,
                'image' => $request->image
            ]);
            $applicationInfoId = $applicationInfo->id;
        }
        //dd($applicationInfoId, $request->all());
        $vehicle_application = new VehicleApplication([
            'application_id' => $applicationInfoId,
            'tyre_size' => $request->tyre_size,
            'class' => $request->class,
            'chasis_no' => $request->chasis_no,
            'fuel_used' => $request->fuel_used,
            'horse_power' => $request->horse_power,
            'rpm' => $request->rpm,
            'seats' => $request->seats,
            'cubic_capacity' => $request->cubic_capacity,
            'standee_no' => $request->standee_no,
            'no_of_cylinders' => $request->no_of_cylinders,
            'weight' => $request->weight,
            'engine_no' => $request->engine_no,
            'body_type' => $request->body_type,
            'makers_country' => $request->makers_country,
            'maker_name' => $request->maker_name,
            'year_of_manufacture' => $request->year_of_manufacture,
            'fit' => 0
        ]);
        $vehicle_application->save();
        
        return back()->with(['success' => 'Saved Successfully']);

    }

    //view driver info
    public function viewDriver($driving_licence){
        $driver =  RegisteredDriver::where('driving_licence_no', $driving_licence)->first();
        return view('adminlte::home.driver_info', compact('driver'));
    }

    //view vehicle info
    public function viewVehicle($vehicle_no){
        $vehicle =  RegisteredVehicle::where('vehicle_registration_no', $vehicle_no)->first();
        return view('adminlte::home.vehicle_info', compact('vehicle'));
    }
        
}
