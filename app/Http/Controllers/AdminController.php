<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Role;

class AdminController extends Controller
{
    //only for admin to view activate/deactivate user page
    public function getDeactivateUser(Request $request)
    {   
        $users = User::latest()->paginate(50);
        
        if($request->has('name')){
            $users = User::where('name', 'like', '%'.$request->name.'%')->orWhere('email', 'like', '%'.$request->name.'%')->paginate(50000);
        }

        return view('adminlte::admin.deactivate-user', compact('users'));
    }

    //only for admin to activate/deactivate user
    public function deactivateUser(Request $request)
    {   //dd($request->all());
        $user = User::where('name', $request->name)->first();
        $user->activated = $request->activated;
        $user->save();

        return back()->with(['success' => 'Successfully done']);
    }

    //only for admin to view assign role to user page
    public function getAssignRoles()
    {   
        $users = User::where('activated', true)->paginate(50);
        return view('adminlte::admin.assign-role-user', compact('users'));
    }

    //only for admin to assign roles to users
    public function postAssignRoles(Request $request)
    {   
        //dd($request->all());
        $user = User::where('name', $request->user)->first();
        $user->roles()->detach();

        $user->roles()->attach(Role::where('name', $request->role)->first());

        return back()->with(['success' => 'Successfully Updated User\'s Role']);
    }

    
}
