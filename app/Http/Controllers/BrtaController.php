<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\{AccidentLocation, ApplicationInformation, DailyVehicle, DriverApplication, DriverPhysicallyFit, RegisteredDriver, RegisteredUserInformation, RegisteredVehicle, User, Role, VehicleApplication};

use Carbon\Carbon;

class BrtaController extends Controller
{
    //only for brta to view activate/deactivate user page
    public function getDeactivateUser(Request $request)
    {   
        $users = User::latest()->paginate(50);
        
        if($request->has('name')){
            $users = User::where('name', 'like', '%'.$request->name.'%')->orWhere('email', 'like', '%'.$request->name.'%')->paginate(50000);
        }

        return view('adminlte::brta.deactivate-user', compact('users'));
    }

    //only for brta to activate/deactivate user
    public function deactivateUser(Request $request)
    {   //dd($request->all());
        $user = User::where('name', $request->name)->first();
        $user->activated = $request->activated;
        $user->save();

        return back()->with(['success' => 'Successfully done']);
    }

    //only for brta to view assign role to user page
    public function getAssignRoles()
    {   
        $users = User::where('activated', true)->paginate(50);
        return view('adminlte::brta.assign-role-user', compact('users'));
    }

    //only for brta to assign roles to users
    public function postAssignRoles(Request $request)
    {   
        //dd($request->all());
        $user = User::where('name', $request->user)->first();
        $user->roles()->detach();

        $user->roles()->attach(Role::where('name', $request->role)->first());

        return back()->with(['success' => 'Successfully Updated User\'s Role']);
    }

    public function viewVehicles(){
            $vehicles = VehicleApplication::all();
           foreach($vehicles as $key=>$vehicle){
              if($vehicle->registeredVehicle==null){
                  $unreg_vehicles[$key]=$vehicle;
              }
           }
       return view('adminlte::brta.vehicles',compact('unreg_vehicles'));

    }
    public function viewRegVehicles(){
         $reg_vehicles = RegisteredVehicle::all();
           
       return view('adminlte::brta.regVehicles',compact('reg_vehicles'));

    }
     public function viewRegFormVehicles($vehicle_id){

          $vehicles = VehicleApplication::all();
           foreach($vehicles as $key=>$vehicle){
              if($vehicle->registeredVehicle==null){
                  $unreg_vehicles[$key]=$vehicle;
              }
           }
       return view('adminlte::brta.regFormVehicles',compact('unreg_vehicles'));

    }
     public function submitRegVehicle(Request $request, $vehicle_id){
        $this->validate($request, [
         // as vehicle and driver will be taken as input
            "vehicle" => "required",
            "registration" => "required",
            //problem
            //"fit" => "required"
        ]);
       // daily_vehicle variable will store the input info of daily vehicle registration. here in the dailyVehicle table a new info will be inserted. Currently logged in vehicle owner's user id will be inserted in owner_id attribute of dailyVehicle table. Current time will be inserted in issued_date attribute of dailyVehicle table. Input of Your vehicle and Driver will be inserted in vehicle_id and driver_id attribute of dailyVehicle table.
        $register_vehicle =  RegisteredVehicle::create([
            'vehicle_id' => $request->vehicle,
            'user_id' => Auth::user()->id,
            'vehicle_registration_no' => $request->registration,
            'issued_date' => Carbon::now()->toDateString(),
            //problem
            'expired_date' => Carbon::now()->toDateString()
        ]);

        return back()->with(['success' => 'Saved Successfully']);
    }


    public function viewDLicenses(){
       return view('adminlte::brta.drivingLicenses');
    }

    public function viewDrivers(){
      $drivers = DriverApplication::all();
           foreach($drivers as $key=>$driver){
              if($driver->registeredDriver==null){
                  $unreg_drivers[$key]=$driver;
              }
           }
       return view('adminlte::brta.drivers',compact('unreg_drivers'));
    }
     public function viewRegDrivers(){
       $reg_drivers = RegisteredDriver::all();
       return view('adminlte::brta.regDrivers',compact('reg_drivers'));
    }
    public function viewRegFormDrivers(){
         $drivers = DriverApplication::all();
           foreach($drivers as $key=>$driver){
              if($driver->registeredDriver==null){
                  $unreg_drivers[$key]=$driver;
              }
           }
       return view('adminlte::brta.regFormDrivers',compact('unreg_drivers'));
    }

    public function submitRegDriver(Request $request){
        $this->validate($request, [
         // as vehicle and driver will be taken as input
            "driver" => "required",
            "license" => "required",
           

        ]);
       // daily_vehicle variable will store the input info of daily vehicle registration. here in the dailyVehicle table a new info will be inserted. Currently logged in vehicle owner's user id will be inserted in owner_id attribute of dailyVehicle table. Current time will be inserted in issued_date attribute of dailyVehicle table. Input of Your vehicle and Driver will be inserted in vehicle_id and driver_id attribute of dailyVehicle table.
        $register_driver =  RegisteredDriver::create([
            
            'driver_id' => $request->driver,
            //problem
            'user_id' => Auth::user()->id,
            'driving_licence_no' => $request->license,
            'issued_date' => Carbon::now()->toDateString(),
            //problem
            'expired_date' => Carbon::now()->toDateString()
        ]);

       

        return back()->with(['success' => 'Saved Successfully']);
    }
    
    public function viewAccidents(){
       return view('adminlte::brta.accidents');
    }
    //problem
      public function viewVehicle($id){
        $unreg_vehicles =  VehicleApplication::where('id', $id)->first();
        return view('adminlte::home.unreg_vehicle_info', compact('unreg_vehicles'));
    }
      public function viewDriver($id){
        $unreg_drivers =  DriverApplication::where('id', $id)->first();
        return view('adminlte::home.unreg_driver_info', compact('unreg_drivers'));
    }
    
}
