<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'activated'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function user_activation()
    {
        return $this->hasMany('App\UserActivation');
    }

    public function cases()
    {
        return $this->belongsToMany('App\Cases', 'case_user', 'user_id', 'case_id');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function caseComments()
    {
        return $this->hasMany('App\CaseComment');
    }

    //check roles for user by user table
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    //return user name
    public function userName($id)
    {
        return $this->where('id',$id)->first()->name;
    }

    //registered driver
    public function registeredDriver()
    {
        return $this->hasMany('App\RegisteredDriver', 'user_id');
    }

    //registered driver
    public function registeredVehicle()
    {
        return $this->hasMany('App\RegisteredVehicle', 'user_id');
    }

    //application information
    public function applicationInformation()
    {
        return $this->hasOne('App\RegisteredUserInformation', 'user_id');
    }

    //notification
    public function notifications()
    {
        return $this->hasMany('App\NotificationUser', 'user_id');
    }

    //daily vehicle owned to this user
    public function dailyVehicle()
    {
        return $this->hasMany('App\DailyVehicle', 'owner_id');
    }
}
