<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model
{
    protected $fillable = [
    	'user_id',
    	'message',
        'type',
        'seen'
    ];

    //user 
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
