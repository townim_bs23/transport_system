<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleApplication extends Model
{
    protected $fillable = [
        'application_id',
    	'tyre_size',
        'class',
        'chasis_no',
        'fuel_used',
        'horse_power',
        'rpm',
        'seats',
        'cubic_capacity',
        'standee_no',
        'no_of_cylinders',
        'weight',
        'engine_no',
        'body_type',
        'makers_country',
        'maker_name',
        'year_of_manufacture',
        'fit'
    ];

    //application information
    public function information()
    {
        return $this->belongsTo('App\ApplicationInformation', 'application_id');
    }

    //registered vehicle
    public function registeredVehicle()
    {
        return $this->hasOne('App\RegisteredVehicle', 'vehicle_id');
    }
}
