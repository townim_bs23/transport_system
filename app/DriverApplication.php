<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverApplication extends Model
{
    protected $fillable = [    	
        'vehicle_class',
        'application_type',
        'licence_type',
        'bank_transaction_no',
        'police_verified',
    ];

    //application information
    public function information()
    {
        return $this->belongsTo('App\ApplicationInformation', 'application_id');
    }

    //driver fitness
    public function driverFitness()
    {
        return $this->hasOne('App\DriverPhysicallyFit', 'driver_application');
    }

    //registered driver
    public function registeredDriver()
    {
        return $this->hasOne('App\RegisteredDriver', 'driver_id');
    }
}
