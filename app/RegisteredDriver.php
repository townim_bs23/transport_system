<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredDriver extends Model
{
    protected $fillable = [
    	'driver_id',
       	'user_id',
        'driving_licence_no',
        'issued_date',
        'expired_date'
    ];

    //driver information
    public function driverInformation()
    {
        return $this->belongsTo('App\DriverApplication', 'driver_id');
    }

    //user information
    public function userInformation()
    {
        return $this->belongsTo('App\User');
    }

    //daily vehicle is drived by this driver
    public function dailyVehicle()
    {
        return $this->hasMany('App\DailyVehicle', 'driver_id');
    }
}
