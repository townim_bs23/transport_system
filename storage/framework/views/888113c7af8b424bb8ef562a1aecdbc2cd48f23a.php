<?php $__env->startSection('htmlheader_title'); ?>
  Owner - All Vehicles
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader'); ?>
##parent-placeholder-5a28aaffcd404e78f33089f625f7f94a2c921dbd##
<style type="text/css">
  
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<div class="box">
  <div class="box-header">
    <h3 class="box-title">List of Your Vehicles</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="vehiclelist" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>SL. No</th>
          <th>Vehicle Licence No</th>
          <th>Fit</th>
          <th>Issued Date</th>
          <th>Expired Date</th>
        </tr>
      </thead>
      <tbody>
        <?php $__currentLoopData = $all_vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td><?php echo e($key+1); ?></td>
          <td><a href="<?php echo e(route('vehicle.info', ['vehicle_no' => $vehicle->vehicle_registration_no])); ?>" target="_blank"><?php echo e($vehicle->vehicle_registration_no); ?></a></td>
          <td><?php echo e($vehicle->vehicleInformation->fit ? 'OK' : 'Not OK'); ?></td>
          <td><?php echo e($vehicle->issued_date); ?></td>
          <td><?php echo e($vehicle->expired_date); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </tbody>
    </table>
  </div>
</div>
          
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script type="text/javascript">
$(document).ready(function(){
  $('#vehiclelist').DataTable();
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>