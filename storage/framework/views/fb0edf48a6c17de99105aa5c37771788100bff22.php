<?php $__env->startSection('htmlheader_title'); ?>
  Owner - Register Daily Vehicle
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader'); ?>
##parent-placeholder-5a28aaffcd404e78f33089f625f7f94a2c921dbd##
<style type="text/css">
  
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<?php if(count($errors) > 0): ?>
<div class="row">
<div class="col-xs-12">
    <div class="alert alert-danger">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
    </div>
</div>    
</div>
<?php endif; ?>


<?php if(Session::has('success')): ?>
<div class="row">
<div class="col-xs-12">
    <div class="alert alert-success">
        <p> <?php echo Session::get('success'); ?> </p>
    </div>
</div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Register Your Vehicle Today</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="" method="post">
        <?php echo csrf_field(); ?>

          <div class="box-body">
            <div class="form-group">
              <label for="vehicle"><b>Your Vehicle : </b></label>
              <select class="form-control" name="vehicle" >
              	<option value="">Select Your Vehicle</option>
              <?php $__currentLoopData = $vehicle_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($vehicle->id); ?>"><?php echo e($vehicle->vehicle_registration_no); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>
            <div class="form-group">
              <label for="driver"><b>Driver : </b></label>
              <select class="form-control" name="driver" >
              	<option value="">Select Your Vehicle's Driver</option>
              <?php $__currentLoopData = $driver_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $driver): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($driver->id); ?>"><?php echo e($driver->driverInformation->information->first_name); ?> <?php echo e($driver->driverInformation->information->last_name); ?> (<?php echo e($driver->driving_licence_no); ?>)</option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary assign">Submit</button>
          </div>
        </form>
      </div>         
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script type="text/javascript">
  
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>