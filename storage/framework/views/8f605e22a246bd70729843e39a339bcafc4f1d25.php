<?php $__env->startSection('htmlheader_title'); ?>
  Driver Information
<?php $__env->stopSection(); ?>

<?php $__env->startSection('htmlheader'); ?>
##parent-placeholder-5a28aaffcd404e78f33089f625f7f94a2c921dbd##
<style>
.image{
  margin-bottom: 20px;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if($driver!=null): ?>
<div class="row">
  <div class="col-md-6">
    <img src="<?php echo e($driver->driverInformation->information->image); ?>" class="img-responsive image">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Personal Information</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <b>Name:</b> <?php echo e($driver->driverInformation->information->first_name); ?> <?php echo e($driver->driverInformation->information->last_name); ?>

      </div><!-- /.box-body -->
       
      <!-- /.box-footer-->
      <div class="box-footer">
        <b>Father's name:</b> <?php echo e($driver->driverInformation->information->father_name); ?>

      </div>
       <div class="box-footer">
        <b>Mother's name:</b> <?php echo e($driver->driverInformation->information->mother_name); ?>

      </div>
       <div class="box-footer">
        <b>Spouse name:</b> <?php echo e($driver->driverInformation->information->spouse_name); ?>

      </div>
       <div class="box-footer">
        <b>Date of birth:</b> <?php echo e($driver->driverInformation->information->birth_date); ?>

      </div>
       <div class="box-footer">
        <b>Email:</b> <?php echo e($driver->driverInformation->information->email); ?>

      </div>
       <div class="box-footer">
        <b>Permanent address:</b> <?php echo e($driver->driverInformation->information->permanent_address); ?>

      </div>
       <div class="box-footer">
        <b>Present address:</b> <?php echo e($driver->driverInformation->information->present_address); ?>

      </div>
       <div class="box-footer">
        <b>Phone number:</b> <?php echo e($driver->driverInformation->information->phone_number); ?>

      </div>
       <div class="box-footer">
        <b>Citizenship:</b> <?php echo e($driver->driverInformation->information->citizen); ?>

      </div>
       <div class="box-footer">
        <b>National ID number:</b> <?php echo e($driver->driverInformation->information->nid); ?>

      </div>
       <div class="box-footer">
        <b>Passport:</b> <?php echo e($driver->driverInformation->information->passport); ?>

      </div>
    </div>
  </div>
  
  <div class="col-md-6">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Physical Fitness Information</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <b>Headache:</b> <?php echo e($driver->driverInformation->driverFitness->headache ? 'OK' : 'Not OK'); ?>

      </div><!-- /.box-body -->
      <div class="box-footer">
        <b>Visual condition:</b> <?php echo e($driver->driverInformation->driverFitness->visual_condition ? 'OK' : 'Not OK'); ?>

      </div>
       <div class="box-footer">
        <b>Traffic sign:</b> <?php echo e($driver->driverInformation->driverFitness->traffic_sign ? 'OK' : 'Not OK'); ?>

      </div>
       <div class="box-footer">
        <b>Hearing:</b> <?php echo e($driver->driverInformation->driverFitness->hearing ? 'OK' : 'Not OK'); ?>

      </div><!-- /.box-footer-->
    </div>
  </div>
  
  <div class="col-md-6">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Application Information</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      
      <div class="box-footer">
        <b>Application type:</b> <?php echo e($driver->driverInformation->application_type); ?>

      </div>
       <div class="box-footer">
        <b>Vehicle class:</b> <?php echo e($driver->driverInformation->vehicle_class); ?>

      </div>
       <div class="box-footer">
        <b>License type:</b> <?php echo e($driver->driverInformation->licence_type); ?>

      </div>
       <div class="box-footer">
        <b>Bank transaction number:</b> <?php echo e($driver->driverInformation->bank_transaction_no); ?>

      </div>
       <div class="box-footer">
        <b>Police verified:</b> <?php echo e($driver->driverInformation->police_verified ? 'OK' : 'Not OK'); ?>

      </div><!-- /.box-footer-->
    </div>
  </div>

  <div class="col-md-6">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Registration Information</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <b>Driver Licence No:</b> <?php echo e($driver->driving_licence_no); ?>

      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <b>Issued date:</b> <?php echo e($driver->issued_date); ?>

      </div>
      <div class="box-footer">
        <b>Expired date:</b> <?php echo e($driver->expired_date); ?>

      </div>
      
      <!-- /.box-footer-->
    </div>
  </div>
</div>
<?php else: ?>
<div class="row">
  <div class="col-md-12">
    <div class="box box-solid box-danger">
      <div class="box-header with-border text-center">
        <h3 class="box-title ">There is no such driver</h3>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::layouts.landing', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>