<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
//use DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_brta = Role::where('name', 'brta')->first();
        $role_user = Role::where('name', 'user')->first();
        $role_malik = Role::where('name', 'malik')->first();
        $role_driver = Role::where('name', 'driver')->first();

        $user = new User();
        $user->name = 'brta';
        $user->email = 'brta@brta.com';
        $user->password = bcrypt('123456');
        $user->activated = true;
        $user->save();
        $last_user_id = DB::table('users')->latest()->value('id');
        $user->roles()->attach($role_brta);


        $user = new User();
        $user->name = 'user';
        $user->email = 'user@user.com';
        $user->password = bcrypt('123456');
        $user->activated = true;
        $user->save();
        $last_user_id = DB::table('users')->latest()->value('id');
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'malik';
        $user->email = 'malik@malik.com';
        $user->password = bcrypt('123456');
        $user->activated = true;
        $user->save();
        $last_user_id = DB::table('users')->latest()->value('id');
        $user->roles()->attach($role_malik);

        $user = new User();
        $user->name = 'driver';
        $user->email = 'driver@driver.com';
        $user->password = bcrypt('123456');
        $user->activated = true;
        $user->save();
        $last_user_id = DB::table('users')->latest()->value('id');
        $user->roles()->attach($role_driver);


        
    }
}
