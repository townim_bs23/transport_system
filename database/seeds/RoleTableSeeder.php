<?php

use Illuminate\Database\Seeder;

use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'brta';
        $role_employee->description = '';
        $role_employee->save();


        $role_admin = new Role();
        $role_admin->name = 'user';
        $role_admin->description = '';
        $role_admin->save();

        $role_oficer = new Role();
        $role_oficer->name = 'driver';
        $role_oficer->description = '';
        $role_oficer->save();

        $role_oficer = new Role();
        $role_oficer->name = 'malik';
        $role_oficer->description = '';
        $role_oficer->save();
    }
}
