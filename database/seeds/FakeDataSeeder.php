<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\{AccidentLocation, ApplicationInformation, DailyVehicle, DriverApplication, DriverPhysicallyFit, RegisteredDriver, RegisteredUserInformation, RegisteredVehicle, User, Role, VehicleApplication};

class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //faker create with BD
        $bn_faker = Faker::create('bn_BD');
        $en_faker = Faker::create();

        //Roles
        $role_brta = Role::where('name', 'brta')->first();
        $role_user = Role::where('name', 'user')->first();
        $role_malik = Role::where('name', 'malik')->first();
        $role_driver = Role::where('name', 'driver')->first();

        //driver
        for($i=1; $i<=20; $i++){
	        //Step 1. Driver application will be filled up with application information
	        $application_information = new ApplicationInformation([
	        	'first_name' => $en_faker->firstName($gender = 'male'|'female'),
		        'last_name' => $en_faker->lastName(),
		        'father_name' => $en_faker->name($gender = 'male'),
		        'mother_name' => $en_faker->name($gender = 'female'),
		        'spouse_name' => $en_faker->name($gender = 'male'|'female'),
		        'gender' => $en_faker->randomElement(['male', 'female']),
		        'email' => $en_faker->freeEmail(),
		        'phone_number' => $bn_faker->phoneNumber(),
		        'birth_date' => $en_faker->date($format = 'Y-m-d', $max = 'now'),
		        'permanent_address' => $bn_faker->address(),
		        'present_address' => $bn_faker->address(),
		        'citizen' => 'Bangladeshi',
		        'nid' => $en_faker->creditCardNumber(),
		        'passport' => $en_faker->creditCardNumber(),
		        'image' => $en_faker->imageUrl($width = 640, $height = 480)
	        ]);
	        $application_information->save();

	        $driver_application = new DriverApplication([
	        	'application_id' => $application_information->id,
	        	'vehicle_class' => $en_faker->word,
		        'application_type' => $en_faker->word,
		        'licence_type' => $en_faker->word,
		        'bank_transaction_no' => $en_faker->creditCardNumber,
		        'police_verified' => $en_faker->boolean
	        ]);
	        $driver_application->save();

	        //Step 2. make some driver fit in DriverPhysicallyFit
	        $physically_fit = new DriverPhysicallyFit([
	        	'driver_application' => $driver_application->id,
		        'headache' => $en_faker->boolean(),
		        'visual_condition' => $en_faker->boolean(),
		        'traffic_sign' => $en_faker->boolean(),
		        'hearing' => $en_faker->boolean()
	        ]);
	        $physically_fit->save();

	        //Step 3. registered some driver in RegisteredDriver and RegisteredUserInformation and user  and attached the role
	        if($i%3==0){
	        	$driver_user = new User([
		        	'name' => $en_faker->userName(), 
		        	'email' => $application_information->email, 
		        	'password' => bcrypt('123456'), 
		        	'activated' => true
		        ]);
		        $driver_user->save();

		        $driver_user->roles()->attach($role_driver);

		        $registered_user_info = new RegisteredUserInformation([
		        	'information_id' => $application_information->id,
    				'user_id' => $driver_user->id
		        ]);
		        $registered_user_info->save();

		        $registered_driver = new RegisteredDriver([
		        	'driver_id' => $driver_application->id,
			       	'user_id' => $driver_user->id,
			       	'driving_licence_no' => $en_faker->unique()->creditCardNumber,
			        'issued_date' => $en_faker->date($format = 'Y-m-d', $max = 'now'),
			        'expired_date' => $en_faker->date($format = 'Y-m-d', $max = 'now')
		        ]);
		        $registered_driver->save();
	        }
	        
        }

        //vehicle owner application
        //Step 4. make one vehicle owner and registered it
        $application_information = new ApplicationInformation([
        	'first_name' => $en_faker->firstName($gender = 'male'|'female'),
	        'last_name' => $en_faker->lastName(),
	        'father_name' => $en_faker->name($gender = 'male'),
	        'mother_name' => $en_faker->name($gender = 'female'),
	        'spouse_name' => $en_faker->name($gender = 'male'|'female'),
	        'gender' => $en_faker->randomElement(['male', 'female']),
	        'email' => $en_faker->freeEmail(),
	        'phone_number' => $bn_faker->phoneNumber(),
	        'birth_date' => $en_faker->date($format = 'Y-m-d', $max = 'now'),
	        'permanent_address' => $bn_faker->address(),
	        'present_address' => $bn_faker->address(),
	        'citizen' => 'Bangladeshi',
	        'nid' => $en_faker->creditCardNumber(),
	        'passport' => $en_faker->creditCardNumber(),
	        'image' => $en_faker->imageUrl($width = 640, $height = 480)
        ]);
        $application_information->save();

        $vehicle_owner = new User([
        	'name' => $en_faker->userName(), 
        	'email' => $application_information->email, 
        	'password' => bcrypt('123456'), 
        	'activated' => true
        ]);
        $vehicle_owner->save();

        $vehicle_owner->roles()->attach($role_malik);

        $registered_user_info = new RegisteredUserInformation([
        	'information_id' => $application_information->id,
			'user_id' => $vehicle_owner->id
        ]);
        $registered_user_info->save();

        //vehicle
        for($i=1; $i<=20; $i++){
        	//Step 5. Vehicle application will be filled up with application information, make some vehicle fit
	        $vehicle_application = new VehicleApplication([
	        	'application_id' => $application_information->id,
	        	'tyre_size' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'class' => $en_faker->randomLetter,
		        'chasis_no' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'fuel_used' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'horse_power' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'rpm' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'seats' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'cubic_capacity' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'standee_no' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'no_of_cylinders' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'weight' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'engine_no' => $en_faker->numberBetween($min = 1000, $max = 9000),
		        'body_type' => $en_faker->word,
		        'makers_country' => $en_faker->country,
		        'maker_name' => $en_faker->name,
		        'year_of_manufacture' => $en_faker->year($max = 'now'),
		        'fit' => $en_faker->boolean
	        ]);
	        $vehicle_application->save();

	        //Step 6. registered some vehicle in RegisteredVehicle 
	        if($vehicle_application->fit==true){
		        $registered_vehicle = new RegisteredVehicle([
		        	'vehicle_id' => $vehicle_application->id,
			       	'user_id' => $vehicle_owner->id,
			       	'vehicle_registration_no' => $en_faker->unique()->creditCardNumber,
			        'issued_date' => $en_faker->date($format = 'Y-m-d', $max = 'now'),
			        'expired_date' => $en_faker->date($format = 'Y-m-d', $max = 'now')
		        ]);
		        $registered_vehicle->save();
	        }

	        //Step 7. make some vehicle registered in DailyVehicle
	    }



        
    }
}

