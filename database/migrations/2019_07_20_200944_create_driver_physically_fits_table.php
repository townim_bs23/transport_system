<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverPhysicallyFitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_physically_fits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('driver_application')->unsigned();
            $table->foreign('driver_application')->references('id')->on('driver_applications')->onDelete('cascade');
            $table->boolean('headache');
            $table->boolean('visual_condition');
            $table->boolean('traffic_sign');
            $table->boolean('hearing');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_physically_fits');
    }
}

