<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('application_id')->unsigned();
            $table->foreign('application_id')->references('id')->on('application_informations')->onDelete('cascade');
            $table->integer('tyre_size');
            $table->string('class');
            $table->string('chasis_no');
            $table->integer('fuel_used');
            $table->integer('horse_power');
            $table->integer('rpm');
            $table->integer('seats');
            $table->integer('cubic_capacity');
            $table->integer('standee_no');
            $table->integer('no_of_cylinders');
            $table->integer('weight');
            $table->integer('engine_no');
            $table->string('body_type');
            $table->string('makers_country');
            $table->string('maker_name');
            $table->integer('year_of_manufacture');
            $table->boolean('fit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_applications');
    }
}
