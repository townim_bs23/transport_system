<?php

/*
**All routes that can viewed by everyone
*/

//home page
Route::get('/', 'HomeController@viewHome');

//view and save application form of driving licence
Route::get('/driving-licence', 'HomeController@viewDriverForm')->name('driver.licence');
Route::post('/driving-licence', 'HomeController@saveDriverInfo');

//view and save application form of vehicle registration
Route::get('/vehicle-registration', 'HomeController@viewVehicleForm')->name('vehicle.registration');
Route::post('/vehicle-registration', 'HomeController@saveVehicleInfo');

//view driver information
Route::get('/driver/{diving_licence}', 'HomeController@viewDriver')->name('driver.info');

//view vehicle owner information
/*need to focus*/
Route::get('/vehicle/{vehicle_no}', 'HomeController@viewVehicle')->name('vehicle.info');

/*
* Authentication 
*/

//Authentication Routes
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//Registration Routes
Route::get('register', [
    'uses' => 'Auth\RegisterController@showRegistrationForm',
  	'as' => 'register'
]);
Route::post('register', [
    'uses' => 'Auth\RegisterController@register',
]);

//Password Reset Routes
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//to handle request when user clicks on the activation link
Route::get('/user/activation/{token}', 'Auth\LoginController@activateUser')->name('user.activate');



/*
* For BRTA
*/

Route::group(['prefix'=>'dashboard', 'middleware' => ['auth', 'roles'], 'roles' => ['brta'] ], function(){
	//to view activate/deactivate user view page
	Route::get('/deactivate-user', [
		'uses' => 'BrtaController@getDeactivateUser',
		'as' => 'deactivate.user',
	]);

	//to activate/deactivate user
	Route::post('/deactivate-user', [
		'uses' => 'BrtaController@deactivateUser',
		'as' => 'deactivate.user',
	]);

	//to view assign role page
	Route::get('/assign-role', [
		'uses' => 'BrtaController@getAssignRoles',
		'as' => 'assign.role',
	]);

	//to assign role to a user
	Route::post('/assign-role', [
		'uses' => 'BrtaController@postAssignRoles',
		'as' => 'assign.role',
	]);

	Route::get('/vehicles', 'BRTAController@viewVehicles')->name('vehicles');
	Route::get('/regVehicles', 'BRTAController@viewRegVehicles')->name('regVehicles');

	Route::get('/regFormVehicles/{vehicle_id}', 'BRTAController@viewRegFormVehicles')->name('regFormVehicles');
	Route::post('/regFormVehicles/{vehicle_id}', 'BRTAController@submitRegVehicle');
	
	Route::get('/driving-licenses', 'BRTAController@viewDLicenses')->name('dLicenses');
	Route::get('/drivers', 'BRTAController@viewDrivers')->name('drivers');
	Route::get('/regDrivers','BRTAController@viewRegDrivers')->name('regDrivers');
	Route::get('/regFormDrivers','BRTAController@viewRegFormDrivers')->name('regFormDrivers');
	Route::post('/regFormDrivers', 'BRTAController@submitRegDriver');
	Route::get('/accidents', 'BRTAController@viewAccidents')->name('accidents');
	//problem
    Route::get('/regVehicle/{vehicle_id}', 'BRTAController@viewVehicle')->name('unreg.vehicle.info');
    Route::get('/regDriver/{driver_id}', 'BRTAController@viewDriver')->name('unreg.driver.info');
});

/*
* For All Users
*/
Route::group(['prefix'=>'dashboard', 'middleware' => ['auth'] ], function(){
	//to view dashboard
	Route::get('/', [
		'uses' => 'HomeController@viewDashboard',
		'as' => 'admin.dashboard',
	]);

	

});


/*
* For Users like rider 
*/
Route::group(['prefix'=>'dashboard', 'middleware' => ['auth', 'roles'], 'roles' => ['user'] ], function(){
	

});


/*
* For Driver 
*/
Route::group(['prefix'=>'dashboard', 'middleware' => ['auth', 'roles'], 'roles' => ['driver'] ], function(){
	//personal information
	Route::get('/info', 'DriverController@viewInfo')->name('dInfo');

});


/*
* For Vehicle Owner 
*/
Route::group(['prefix'=>'dashboard', 'middleware' => ['auth', 'roles'], 'roles' => ['malik'] ], function(){
	
	Route::get('/all-vehicles', 'VehicleOwnerController@viewAllVehicles')->name('all.vehicles');
    Route::get('/registered-vehicles', 'VehicleOwnerController@viewRegisteredVehicles')->name('RVehicles');

    //register vehicle to a driver daily
    Route::get('/owner/daily-vehicle', 'VehicleOwnerController@getDailyVehicle')->name('register.daily-vehicle');
    Route::post('/owner/daily-vehicle', 'VehicleOwnerController@submitDailyVehicle');

});


